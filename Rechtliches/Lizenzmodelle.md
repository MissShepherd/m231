# Shrinkwaps, EULA, Clickwrap:
Eine Clickwrap-Vereinbarung, auch bekannt als Click-Through, Shrink-Wrap oder Sign-in-Wrap, ist eine Online-Vereinbarung, bei der der Benutzer seine Zustimmung erklärt, indem er auf eine Schaltfläche klickt oder ein Kästchen mit der Aufschrift „Ich stimme zu“ ankreuzt. Zweck einer Clickwrap-Vereinbarung ist die digitale Erfassung der Vertragsannahme.

Click-Through-Vereinbarungen ermöglichen es Unternehmen, einen Vertrag mit Kunden abzuschließen, ohne mit jedem Benutzer einzeln zu verhandeln. Um als legitim zu gelten, muss der Vertrag:

- Für alle Benutzer angemessen und gut sichtbar sein.
- Benötigen Sie eine aktive, bestätigende Zustimmung.
- Für den durchschnittlichen Benutzer leicht verständlich sein.
- Durchsetzbar sein.

Clickwrap-Vereinbarungen sind ein digitaler Ableger von Schrumpffolienlizenzen. Im letzten Jahrhundert, als die meiste Software lokal installiert wurde, wurden Schrumpfverpackungslizenzen häufig von Softwareanbietern verwendet, um ihr geistiges Eigentum zu schützen. Als der Kunde die Plastikschrumpffolie entfernte, die eine neue Software-Diskette oder -CD schützte, erklärte er vertraglich, dass er den Nutzungsbedingungen des Softwareanbieters zustimmte.

# Standardlizenz (Concurrent Use Lizenz):

Die Standardlizenz ist ein standardmäßiger Lizenztyp, der es Ihnen erlaubt, eine heruntergeladene Datei für persönliche und kommerzielle Zwecke zu verwenden, die unter die Bedingungen der Standardlizenz fallen.
## Rechtliche Angaben auf Websites

Jede geschäftliche Webseite benötigt ein sogenanntes „Impressum“ mit Angaben zu Identität und Kontaktdaten des Seitenanbieters. Das Impressum sollte auf einer eigenen Unterseite als wiederkehrender Link mit Bezeichnung „Impressum“ erscheinen. Am besten auf jeder Seite, z.B. in der Kopf-/Fußzeile oder Seitenleiste, mindestens aber muss es mit 2 Klicks erreichbar sein.
Pflichtangaben gemäß §5 Telemediengesetz sind:

- Name (Person, Firma, Unternehmensname)- - Rechtsform (z.B.: GbR, GmbH, e.K., AG)
- Vertretungsberechtigte Person(en) mit Vor- und - Nachname (bei Kaptialgesellschaften z.B. - Geschäftsführer oder Vorstand)
- Ladungsfähige Anschrift (kein Postfach!)
- Kontaktdaten (Telefon, E-Mail, evtl. Fax)
- Angaben zum Register (Amtsgericht und Nummer, - z.B. Handelsregister, Vereinsregister)
- Umsatzsteuer-Identifikations-Nummer - nur - sofern vorhanden (nicht erforderlich ist die - Steuernummer!)
- Berufsaufsichtsbehörde mit Adresse (bei - Gewerbe mit behördlicher Genehmigung)
- Berufspezifische Informationen (bei - zulassungspflichtigen Berufen mit bes. - Qualifikation)

Quelle: [IHK München](https://www.ihk-muenchen.de/de/Service/Recht-und-Steuern/Internetrecht/Rechtssichere-Internetseite/)

<br>


[![Erstes Bild](https://i.postimg.cc/QCcph1QQ/Screenshot-2022-11-07-160124.png)](https://postimg.cc/6457h71q)

[![Zweites Bild](https://i.postimg.cc/8PqqyS5x/Screenshot-2022-11-07-160523.png)](https://postimg.cc/Yhzn9TQx)


[![Drittes Bild](https://i.postimg.cc/XNZWxxLG/Screenshot-2022-11-07-160602.png)](https://postimg.cc/LhMG8tr2)
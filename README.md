# M231 

- **Webseite mit Markdown gestalten [Pinguine](Diverses/Pinguine.md)** <br>
Hier haben wir eine Webseite in Markdown nachgestellt.

- **Webseite verwendet Cookies [Cookies](Daten/Cookies.md)** <br>Hier haben wir auf dieversen Webseiten geguckt was es für Cookies gibt und wie wir damit umgehen sollen.

- **Wo speichere ich welche Daten ab: Auftrag [Speichern](Daten/Speichern.md)**<br>
Hier habe ich eine kurze Liste zusammengestellt was ich wie auf meinem Handy speichere.

- **Checklisten für Daten [Checkliste](Daten/Checklisten%20des%20Datenschutzbeauftragte.md)**<br>
Hier habe ich eine Liste bekommen, die ich ausgefüllt habe und so sah ich wie und was ich wo speichere oder was ich noch verbessern sollte.

- **Passwort hacken [Passwort hacken](Passwort%20topic/Passwort%20hacken.md)**<br>
Wir haben gemeinsam in einer Gruppe Passwörter zu knacken bekommen und jemand hat dann bemerkt, dass es an diesem Tag nicht möglich war die Passwörter zu knacken da es der letzte Tag im Monat war und der Generator nur 29 Passwörter generiert.

- **Erklären der Begriffe Authentifizieren/- Autorisieren [Authentifizieren/ Autorisieren](Passwort%20topic/Authentifizierung%20vs%20Autorisierung.md)**<br>
Hier ist eine Erklärung zum Unterschied zwischen Authentifizieren und Autorisieren.

- **Das Internet als Datenarchiv [Daten](Daten/Daten.md)**<br>
Hier haben wir zu einem Artikel in der Zeitung nachgeforscht und sehr viel über die Personen, die im Artikel zensiert waren, herrausgefunden.

- **Backupverfahren [Backup](Daten\Backup.md)**<br>
Eine Erklärung zu Backups.

- **Passwort-Manager [Passwort](Passwort%20topic/Passwortmanager.md)**<br>
Hier habe ich was zu Passwortmanager zusammengestellt und welche aus einem Test aufgelistet.

- **Verschlüsselungs-Auftrag [Verschlüsselung](Daten/Verschl%C3%BCsselung.md)**<br>
Hier haben wir in Gruppen Texte versschlüsselt und es verschlüsselt verschickt.

- **Rechtliche Angaben auf Websites [Rechtliche Angaben](Rechtliches/Rechtliche%20Angaben%20auf%20Websites.md)**<br>
Hier hat es wichtige Angaben worauf Webseiten Besitzer achten sollten.

- **Software-Lizenzen [Lizenzmodelle](Rechtliches/Lizenzmodelle.md)**<br>
Hier hat es Auskunft zu Lizenzmodelle
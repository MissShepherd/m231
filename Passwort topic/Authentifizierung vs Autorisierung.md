# Authentifizierung – das Nachweisen einer Identität 

Wenn ich zum Beispiel Tram fahre und der Kontrolleur kommt authentifiziere ich mich in dem ich ihm mein Swiss Pass zeige. 
 
# Autorisierung – das Gewähren des Zugangs zu den Privilegien, welche der erfolgreich nachgewiesenen Identität zustehen 

In diesem Fall gibt der Kontrolleur mir so die Autorisierung das Tram und die ÖV so zu nutzen. 
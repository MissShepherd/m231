## Passwort hacken Powershell
In diesem Auftrag sollen wir mithilfe der Brute-Force-Attacke von einer Website "unser" Passwort hacken.

Wir haben ein PowerShell-Skript bekommen, welches wir zuerst zum Laufen bringen mussten. Dann sollten wir es so anpassen, dass wir unser und das Passwort möglichst vieler Klassenkameraden herausfinden konnten.

Mein Passwort ist Qwerty123

Ich bin folgendermassen vorgegangen, ich habe den Namen jeder Person in das Skript geschrieben. Dann habe ich Listen der häufigsten Passwörter eingefügt und ausprobiert. Dann habe ich das Programm so lange laufen lassen, bis es mein Passwort herausfand.
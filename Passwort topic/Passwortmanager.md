# Passwortmanager

## Passwortmanager-Test 2022

Testsieger 1Password ist dabei das Maß aller Dinge. Die Software bietet richtig starke Sicherheit und sehr einfache Bedienung. Pro Jahr kostet Sie das rund 30 Euro. 

Quelle: [CHIP](https://www.chip.de/artikel/Test-Die-besten-Passwort-Manager_182620837.html)

- AgileBits 1Password - sehr gut (1,2)
- Enpass Individual - sehr gut (1,4)
- Bitwarden Premium -sehr gut (1,5)
- Dashlane Premium gut (1,6)

## Was sind die Aufgaben eines Passwortmanagers?

Wenn wir ein besseres Gedächtnis hätten, wären Passwortmanager überflüssig. Aber die Sache ist, dass man erst ab acht Stellen mit einem Mix aus Buchstaben, Zahlen und Sonderzeichen anfangen kann, von einem einigermaßen sicheren Passwort zu sprechen. Das BSI empfiehlt für WLAN-Passwörter gar mindestens 20 Stellen. Hinzu kommt, dass jedes Passwort nur genau einmal verwendet werden soll. So gesehen sind Passwortmanager also in erster Linie Merkhilfen für Ihre Passwörter.



(Ich bin leider nicht dazu gekommen, die Manager zu testen)
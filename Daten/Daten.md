# Informationen vom Artikel entnommen

- Opfer und Täter 22 Jahre alt.
- Opfer wurde in Hamburg getötet, wo sie kurz davor war Musicaldarstellerin zu werden.
- Opfer ist in der Ostschweiz aufgewachsen.
- Täter war Jungpolitiker aus dem Thurgau.
- Täter verfolgte Opfer zweimal nach Hamburg und konfrontierte sie vor der Wohnung.
- Täter war beliebt und bezeichnete sich als "Brückenbauer".
- Wir haben Bilder für reverse Image searches im Internet.

# Informationen online gefunden
Auffindeort: Hamburg-Ottensen, Scheel-Plessen-Straße https://www.presseportal.de/blaulicht/pm/6337/5190138

Bild der trauernden Famile, deutsche News-Portale haben mehr Info und weniger Zensur als schweizer Magazine. 
![alt text](https://images.bild.de/62501379435b2d2bcc47fa89/6db403840ece37777ec6ec7f2c0d1703/5/2?w=992)

Echter Name des Täters: Kim Wick, Jungpolitiker(VCP)
![alt text](https://images.bild.de/62501379435b2d2bcc47fa89/dfa35f87045b63ab011ccf6fc0edc824/1/2?w=1984)

Link zu Antonia Herings Instagram: https://www.instagram.com/antonia_._luisa/?hl=de


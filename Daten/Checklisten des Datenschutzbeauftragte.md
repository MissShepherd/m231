# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

# Da­ten im Ho­me­of­fice schüt­zen
 - Link: https://www.datenschutz.ch/meine-daten-schuetzen/daten-im-homeoffice-schuetzen
1. Installieren Sie alle Updates
    -  ich bin immer up to date damit alles sicher bleibt
2. Verwenden Sie starke Passwörter
    - Ich verwende starke generierte Passwörter
3. Geben Sie Passwörter nicht weiter
    - Ich gebe auch auf anderen Geräten mein Passwort selbst ein zB wenn jemand Wlan bei mir haben möchte.
4. Schützen Sie Personendaten und geschäftliche Informationen
    - Die Daten die ich auf der Arbeit habe sind mir nur Remote verfügbar und ich habe nichts auf meinen Privaten Geräten.
5. Setzen Sie E-Mail sicher ein
    - Ich habe für geschäftliches eine separate Email Adresse damit alles seine Ordnung hat.
6. Wählen Sie die Kommunikations-Tools bewusst aus
    - Ich schaue das ich nur Tools nutze die von Offiziellen Anbieter kommen 
7. Schützen Sie sich vor Phishing und anderen Bedrohungen
    - ...
8. Melden Sie Datenverlust sofort
    - Wird direkt gemeldet
# “Webseite verwendet Cookies” 

 

## Was sind Cookies? 


Cookies sind Dateien, die von Websites erstellt werden, die Sie besuchen. Mit ihnen werden Browserdaten gespeichert, wodurch Nutzer komfortabler surfen können. Mithilfe von Cookies können Sie auf Websites angemeldet bleiben und diesen ermöglichen, Ihre Einstellungen zu speichern und Ihnen auf Ihren Standort zugeschnittene Inhalte zu bieten. 

Es gibt zwei Arten von Cookies: 

Eigene Cookies werden von der Website erstellt, die Sie besuchen. Die Website wird in der Adressleiste angezeigt. 

Drittanbieter-Cookies werden von anderen Websites erstellt. Diesen Websites gehören einige der Inhalte, wie z. B. Werbeanzeigen oder Bilder, die Sie auf der besuchten Webseite sehen. <br>

 
1. Die meisten Möglichkeiten, die uns bis jetzt begegnet sind, waren: Annehmen, Ablehnen, nur essenzielle annehmen, personalisieren und mehr Informationen. <br><br>
[![FCookie](https://i.postimg.cc/Xvh2mBdp/FotoJet.png)](https://postimg.cc/4K6bhd8g)<br>


2. damit die Seiten Besitzer transparenter sind. Es ist in den meisten Ländern so und wird verlangt, dass man anzeigt, was die Seite mit den Daten der User machen will. 

3. Informationen zum Datenschutz und den andren Angaben die die Seite von uns benötigt. Es vereinfacht das User Erlebnis auf der jeweiligen Seite. Merkt sich auch welche Dinge uns interessieren und schlägt sie öfters vor. Die Daten werden aber auch verkauft und können auch zu Missbrauch führen.  

4. Wir würden den Nutzern raten, dass sie nicht immer sofort auf Annehmen klicken und lieber schauen, was sie überhaupt annehmen. 


5. Man kann Cookie Daten löschen und im jeweiligen Browser angeben, ob man Cookies zu lassen will oder sie von Anfang an schon blockieren will. Ich habe dazu diesen Artikel [source ](https://bit.ly/3KmDTzw)gefunden. 

 
7. “Datenkrake ist ein Schlagwort aus der politischen Diskussion um den Datenschutz. Mit dem Bild eines Kraken werden zahlreiche, weit reichende „Arme“ assoziiert. Das Schlagwort steht für Systeme und Organisationen, die personenbezogene Informationen in großem Stil auswerten oder sie an Dritte weitergeben und dabei Datenschutzbestimmungen oder Persönlichkeitsrechte verletzen.” - Wikipedia 

 

Fazit: Bis jetzt habe ich mir nie viele Gedanken um die Cookie Banner gemacht und habe meist nur die essenziellen angenommen. Nun weiss ich mehr darüber und dass ich nun öfters noch gucken werde, was die Webseite genau mit den Cookies machen will. 
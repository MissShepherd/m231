# Backupverfahren

1. **Was ist überhaupt ein “Backup”?** <br><br>
 Ein Backup ist an sich ein einfacher Vorgang, der sich folgendermaßen definieren lässt: Mit einem Backup lassen sich Daten auf einem geeigneten Speichermedium (z. B. einer externen Festplatte) sichern. Am Ende eines Backups steht eine Sicherheitskopie zur Verfügung, die wichtige Daten in sogenannter redundanter Form enthält, also als Dopplung. Werden alte Daten benötigt oder gibt es einen plötzlichen Datenverlust, lässt sich über das Backup der Datenbestand ganz oder teilweise wiederherstellen. Dies kann durch manuelle Methoden (z. B. Zurückkopieren einzelner Dateien) oder durch spezielle Backup-Software mit Restore-Funktionen erfolgen.

2. **Welche Gründe gibt es, Backups zu erstellen?** <br><br>
Es kann immer passieren dass man sein Device verliert oder es beschädigt wird und dadurch die Daten verloren gehen. Das mächte man ja nicht und so sollte man eine Kopie der Daten als Backup hinterlegen. Egal ob in einer Cloud oder auf dem Heim PC

3. **Worauf kann gesichert werden und welche Vor-/Nachteile gibt es dabei?**
Man kann es auf einem Heim PC sichern, wobei da das Risiko bestehen kann das der auch beschädigt werden kann. <br>
Man kann es in der Cloud sichern, wo es online überall abrufbar ist, was aber das Risiko birgt dass man gehaked wird und eine andere Person Zugriff erhält.

4. **Welche Backup-Verfahren gibt es? Erklären Sie diese möglichst genau!** <br><br>
Es existieren drei verschiedene Arten ein Backup durchzuführen: Vollbackup, inkrementelles Backup und differenzielles Backup. Eine vollständige Sicherung des Datenbestandes bezeichnet man als Vollbackup. <br>
- Ein vollständiges Backup fasst jede einzelne Datei auf einem System in eine Backup-Datei zusammen. Da das komplette Dateisystem eines Servers gesichert wird, beansprucht diese Backup Art viel Zeit in der Erstellung. Wichtig deshalb: einen geeigneten Zeitpunkt für die Erstellung wählen, z.B. den Betriebsschluss oder einen Sonntag. 
- Ein differenzielles Backups baut auf einem vollständigen Backup auf. Von dieser Backup-Art ist immer dann die Rede, wenn die Differenz zum letzten Vollbackup gesichert wird. Dass heißt: Alle Daten, die sich seit dem letzten vollständigen Backup geändert haben oder neu hinzugekommen sind, werden in einer Backup-Datei zusammengefasst. Vom Vortag geänderte Daten werden auf den aktuellen Tag überschrieben.
- Ein inkrementelles Backup setzt ein Vollbackup voraus und sichert nur die Daten, die sich seit dem jeweils letzten Backup geändert haben. Im Gegensatz zum differenziellen Backup werden die Änderungen vom Vortag nicht auf den aktuellen Tag übertragen. Dadurch, dass bei einem inkrementellen Backup immer nur die Daten gesichert werden, die sich innerhalb eines Tages geändert haben, verbraucht diese Backup Art am wenigsten Speicherplatz.


5. **Was wird unter dem “Generationsprinzip” verstanden?**<br><br>
 Backup-Prinzip Großvater – Vater – Sohn
 Bei diesem Backup-Prinzip kommen dreierlei Backup-Varianten zum Einsatz: Sohn, Vater und Großvater (Tages-, Wochen-, Monatssicherung).

6. **Wie halten Sie es mit Backups? Machen Sie welche? Wie? Wann?** <br>
Bei mir werden vom Handy aus automatische Backups von Bildern gemacht und ich sollte dass auch noch mit den restliche Daten in angriff nehmen.

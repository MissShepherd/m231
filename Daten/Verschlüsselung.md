## Verschlüsselung

Unsere Gruppe bestand aus Julian Hollenbach und mir. 

Wir haben erstmal Kleopatra gedownloaded und installiert. Dann haben wir uns per Trial and error durch die Anwendung gearbeitet. Wir haben dann mit Hilfe der Anleitung gemerkt wie wir einen anderen User dazu nehmen kann.

1. Ein Zertifikat erstellen
2. Auf das eigene Profil gehen und es exportieren
3. Den Public Key dann per Email an den Gruppenpartner senden
4. Der Public Key muss dann importiert werden
5. Man kann nun eine Nachricht erstellen und dann verschlüsselt dem Partner zukommen lassen